<?php
/**
 * @file
 * Install, update and uninstall functions for the sina_weibo module.
 *
 */

/**
 * Implements hook_schema().
 */
function sina_weibo_schema(){
  $schema['sina_weibo_account'] = array(
    'description' => "Stores information on specific SinaWeibo user accounts.",
    'fields' => array(
      'weibo_uid' => array(
        'description' => "The unique identifier of the {sina_weibo_account}.",
    	'type' => 'numeric',
    	'unsigned' => TRUE,
    	'precision' => 20,
    	'scale' => 0,
    	'not null' => TRUE,
    	'default' => 0,
      ),
      'uid' => array(
        'description' => "The {users}.uid of the owner of this account",
    	'type' => 'int',
    	'unsigned' => TRUE,
    	'size' => 'big',
    	'not null' => TRUE,
      ),
      'screen_name' => array(
        'description' => "The unique login name of the {sina_weibo_account}",
    	'type' => 'varchar',
    	'length' => 255,
      ),      
      'name' => array(
        'description' => "The full name of the {sina_weibo_account} user.",
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'access_token' => array(
        'description' => "The token_key for oauth-based access.",
    	'type' => 'varchar',
    	'length' => 255,
      ),
      'expires_in' => array(
        'description' => "The {sina_weibo_account}.access_token expires time.",
    	'type' => 'int',
    	'unsigned' => TRUE,
    	'not null' => TRUE,
      ),
      'last_refresh' => array(
        'description' => "A UNIX timestamp marking the date SinaWeibo statuses were last fetched on.",
        'type' => 'int',
    	'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'description' => array(
        'description' => "The description/biography associated with the {sina_weibo_account}.",
        'type' => 'varchar',
        'length' => 255,
      ),
      'location' => array(
        'description' => "The location of the {sina_weibo_account}'s owner.",
        'type' => 'varchar',
        'length' => 255,
      ),
      'followers_count' => array(
        'description' => "The number of users following this {sina_weibo_account}.",
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'friends_count' => array(
        'description' => "The number of users this {sina_weibo_account} is following.",
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'statuses_count' => array(
        'description' => "The total number of status updates performed by a user, excluding direct messages sent.",
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'favourites_count' => array(
        'description' => "The  number of statuses a user has marked as favorite.",
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'url' => array(
        'description' => "The url of the {sina_weibo_account}'s home page.",
        'type' => 'varchar',
        'length' => 255,
      ),
      'profile_image_url' => array(
        'description' => "The url of the {sina_weibo_account}'s profile image.",
        'type' => 'varchar',
        'length' => 255,
      ),
    ),
    'indexes' => array('screen_name' => array('screen_name')),
    'primary key' => array('weibo_uid'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function sina_weibo_install() {
  // Set the weight to 3, making it heavier than Pathauto.
  db_update('system')
    ->fields(array(
        'weight' => 3,
      ))
    ->condition('type', 'module')
    ->condition('name', 'sina_weibo')
    ->execute();
}