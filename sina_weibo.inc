<?php
module_load_include('class.php', 'sina_weibo');

function sina_weibo_connect(){    
    $key = variable_get('sina_weibo_consumer_key', '');
    $secret = variable_get('sina_weibo_consumer_secret', '');
    $c = new SaeTClientV2( $key , $secret , $_SESSION['sina_weibo']['access_token'] );
    $result = $c->get_uid();
    if(isset($result['error_code'])){
        drupal_set_message(t($result['error']),'error');
        drupal_goto(url('/'));
    }else{
        return $c;
    }    
}

function sina_weibo_signin_redirect(){    
    $key = variable_get('sina_weibo_consumer_key', '');
    $secret = variable_get('sina_weibo_consumer_secret', '');
    $callback = url('sina_weibo/auth_callback', array('absolute' => TRUE));
    $o = new SaeTOAuthV2( $key , $secret );
    $code_url = $o->getAuthorizeURL( $callback );
    
    drupal_goto($code_url);
}

function sina_weibo_auth_callback(){
    $key = variable_get('sina_weibo_consumer_key', '');
    $secret = variable_get('sina_weibo_consumer_secret', '');
    $o = new SaeTOAuthV2( $key , $secret );
    
    if (isset($_REQUEST['code'])) {
        $keys = array();
        $keys['code'] = $_REQUEST['code'];
        $keys['redirect_uri'] = url('sina_weibo/auth_callback', array('absolute' => TRUE));
        try {
            $token = $o->getAccessToken( 'code', $keys ) ;
        } catch (OAuthException $e) {
            return t('Login failed!');
        }
    }
    
    if ($token) {
        $_SESSION['sina_weibo'] = $token;
        sina_weibo_logged($token);
        
    } else {
        return t('Login failed!');    
    }
    return t('Welcome..');
}

function sina_weibo_logged($token){
    global $user;
    $weibo = sina_weibo_connect();
    $uid_get = $weibo->get_uid();
    $weibo_uid = $uid_get['uid'];
    $profile = $weibo->show_user_by_id($weibo_uid);
    $profile['expires_in'] = $token['expires_in'];
    $profile['access_token'] = $token['access_token'];
    $profile['last_refresh'] = time();
    if($uid = db_query("SELECT uid FROM {sina_weibo_account} WHERE weibo_uid = :weibo_uid", array(':weibo_uid' => $weibo_uid))->fetchField()){
        $user = user_load($uid);
        sina_weibo_account_save($profile);        
    }else{        
        if(variable_get('sina_weibo_signin_register', 0)){
            $_users = user_load_multiple(array(), array('name' => $profile['screen_name']));
            $account = array_shift($_users);
            if(empty($account->uid)){
                $edit = array(
                    'name' => $profile['screen_name'],
                    'pass' => user_password(),
                    'init' => $profile['screen_name'],
                    'status' => 1,
                    'access' => REQUEST_TIME,
                );
                $account = user_save('', $edit);
                $user = $account; 
                sina_weibo_account_save($profile);
            }else{
                drupal_set_message(t('The nickname %name is already in use on this site, please register below with a new nick name, or @login to continue.', array('%name' => $profile['screen_name'], '@login' => url('user/login'))), 'warning');
            }
        }else{
            $_SESSION['sina_weibo']['values'] = $profile;
            drupal_set_message(t('Please complete the following registration form to create your new account on %site', array('%site' => variable_get('site_name', ''))));
            drupal_goto('user/register');
        }
    }    
}

/**
 * Saves a SinaWeiboUser object to {sina_weibo_account}
 */
function sina_weibo_account_save($profile,$account = NULL){
    $values = (array) $profile;
    $values['weibo_uid'] = $values['id'];
    if (empty($account)) {
        global $user;
        $account = $user;
    }
    $values['uid'] = $account->uid;

    $schema = drupal_get_schema('sina_weibo_account');
    foreach ($values as $k => $v) {
        if (!isset($schema['fields'][$k])) {
            unset($values[$k]);
        }
    }

    db_merge('sina_weibo_account')
        ->key(array('weibo_uid' => $values['weibo_uid']))
        ->fields($values)
        ->execute();
        
    $user->sina_weibo_accounts = $values;
}

/**
 * Load a SinaWeibo account from {sina_weibo_account}.
 *
 * @param $id
 *   SinaWeibo UID
 *
 * @return
 *   object
 *
 */
function sina_weibo_account_load($id) {
  if ( $values = db_query("SELECT * FROM {sina_weibo_account} WHERE weibo_uid = :weibo_uid", array(':weibo_uid' => $id)) ) {
    return $values;
  }
}

/**
 * Delete a user from {sina_weibo_account}.
 *
 * @param $weibo_uid
 *   An integer with the SinaWeibo UID.
 *
 * @param $screen_name
 *   Optional string with the user name.
 */
function sina_weibo_account_delete($weibo_uid, $screen_name = NULL) {
  $query = db_delete('sina_weibo_account');
  $query->condition('weibo_uid', $weibo_uid);
  if (!empty($screen_name)) {
    $query->condition('screen_name', $screen_name);
  }
  $query->execute();
}