<?php

/**
 * admin setting
 */
function sina_weibo_admin_form(){
  $form['oauth'] = array(
    '#type' => 'fieldset',
	'#title' => t('OAuth Settings'),
	'#description' => t('To enable OAuth based access for SinaWeibo,you must <a href="@url">register you application</a> with SinaWeibo and add the provided keys here.',array('@url'=>'http://open.weibo.com/'))
  );
  $form['oauth']['callback_url'] = array(
    '#type' => 'item',
    '#title' => t('Callback URL'),
    '#markup' => url('sina_weibo/auth_callback', array('absolute' => TRUE)),
  );
  $form['oauth']['sina_weibo_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Consumer key'),
    '#default_value' => variable_get('sina_weibo_consumer_key', NULL),
  );
  $form['oauth']['sina_weibo_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Consumer secret'),
    '#default_value' => variable_get('sina_weibo_consumer_secret', NULL),
  );
  return system_settings_form($form);
}

/**
 * returns an image link for signing in with sinaweibo
 */
function sina_weibo_signin_admin_settings($form, &$form_state) {
  $form = array();

  $img_path = drupal_get_path('module', 'sina_weibo') . '/images/login';
  $results = file_scan_directory($img_path, '/.png/');

  $options = array();
  foreach ($results as $image) {
    $options[$image->filename] = theme('image', array('path' => $image->uri));
  }

  $form['sina_weibo_signin_button'] = array(
    '#type' =>  'radios',
    '#title' => t('Select sign-in button'),
    '#options' => $options,
    '#default_value' => variable_get('sina_weibo_signin_button', '32.png'),
  );

  $form['sina_weibo_signin_register'] = array(
    '#title' => t('Automatically register new users'),
    '#type' => 'radios',
    '#options' => array(t('No'), t('Yes')),
    '#default_value' => variable_get('sina_weibo_signin_register', 0),
    '#description' => t('Warning, if you enable this, new user accounts will be created without email addresses.'),
  );

  return system_settings_form($form);
}


/**
 * 
 */
function sina_weibo_user_settings($account) {
  module_load_include('inc', 'sina_weibo');

  $output = array();
  if (!empty($account->sina_weibo_accounts)) {
    $output['list_form'] = drupal_get_form('sina_weibo_account_list_form', $account->sina_weibo_accounts);
  }
  $output['form'] = drupal_get_form('sina_weibo_account_form', $account);

  return $output;
}

/**
 * 
 */
function sina_weibo_account_list_form($form, $form_state, $sina_weibo_accounts = array()) {
  $form['#tree'] = TRUE;
  $form['accounts'] = array();

  foreach ($sina_weibo_accounts as $sina_weibo_account) {
    $form['accounts'][] = _sina_weibo_account_list_row($sina_weibo_account);
  }

  if (!empty($sina_weibo_accounts)) {
    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save changes'),
    );
  }

  return $form;
}

function _sina_weibo_account_list_row($account) {
  $form['#account'] = $account;

  $form['id'] = array(
    '#type' => 'value',
    '#value' => $account->id,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );

  $form['screen_name'] = array(
    '#type' => 'value',
    '#value' => $account->screen_name,
  );

  $form['image'] = array(
    '#markup' => theme('image', array('path' => $account->profile_image_url)),
  );

  $form['visible_name'] = array(
    '#markup' => l($account->screen_name, 'http://www.weibo.com/' . $account->screen_name),
  );

  $form['description'] = array(
    '#markup' => filter_xss($account->description),
  );
/*
  $form['protected'] = array(
    '#markup' => empty($account->protected) ? t('No') : t('Yes'),
  );

  // Here we use user_access('import own tweets') to check permission instead
  // of user_access('import own tweets', $account) is because we allow roles
  // with sufficient permission to overwrite the user's import settings.
  if (user_access('import own tweets')) {
    $form['import'] = array(
      '#type' => 'checkbox',
      '#default_value' => user_access('import own tweets') ? $account->import : '',
    );
  }
*/
  $form['delete'] = array(
    '#type' => 'checkbox',
  );

  return $form;
}


/**
 * 
 */
function sina_weibo_account_form($form, $form_state, $account = NULL) {
  if (empty($account)) {
    global $user;
    $account = $user;
  }

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );

  if (_twitter_use_oauth()) {
    $form['#validate'] = array('twitter_account_oauth_validate');
  }
  else {
    $form['screen_name'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('Twitter user name'),
    );

    $form['import'] = array(
      '#type' => 'checkbox',
      '#title' => t('Import statuses from this account'),
      '#default_value' => TRUE,
      '#access' => FALSE,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add account'),
  );

  return $form;
}

